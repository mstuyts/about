# About

This account contains all my projects I store on [GitLab](https://gitlab.com/mstuyts/).  My GIS related projects can be found on https://gitlab.com/GIS-projects/ and all other projects on https://gitlab.com/mstuyts/. 


A list of all my GitLab projects can be found on https://mstuyts.gitlab.io/.


My personal webpage can be found on https://michelstuyts and my GIS and Geospatial related blog on https://stuyts.xyz.